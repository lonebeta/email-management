export interface ReferenceInterface {
    referenceId:string,
    referenceType:string
}

export interface PayloadInterface {
    source:string,
    reportType:string,
    message:string,
    reportId:string,
    referenceResourceId:string,
    referenceResourceType:string
}

export interface ElementInterface {
    id:string,
    source:string,
    sourceIdentityId:string,
    reference:ReferenceInterface,
    state:string,
    payload:PayloadInterface,
    created:string
}

export interface ReportsInterface {
    size:number,
    nextOffset:string,
    elements:ElementInterface[]
}
