import React, { useState, useEffect } from 'react';
import {ElementInterface} from 'components/Interface/Reports';

const sendBlockRequest = (reportId:string) => {
    fetch(`http://localhost:8000/reports/${reportId}`, { 
      method: 'put',
      headers: new Headers({
        'Content-Type':'application/json'
      }),
      body: JSON.stringify({state:"BLOCKED"})
    })
    .then(response => response.json())
    .then((data) => console.log("Block Request"));
}

const sendResolveRequest = (reportId:string) => {
    fetch(`http://localhost:8000/reports/${reportId}`, { 
      method: 'put',
      headers: new Headers({
        'Content-Type':'application/json'
      }),
      body: JSON.stringify({ticketState:"CLOSED"})
    })
    .then(response => response.json())
    .then((data) => console.log("Block Request"));
}

const ReportsTableRow: React.FC<ElementInterface> = (report) => {
    return (
        <div className='grid grid-cols-12 border border-grey-200 p-2 border-sm mb-3 rounded-xl shadow-md'>
            <div className='col-span-5 pt-4 pb-4'>
                <div className='w-full'><b>ID:</b> {report.id}</div>
                <div className='w-full'><b>State:</b> {report.state}</div>
                <div className='w-full'><a href="#">Details</a></div>
            </div>
            <div className='col-span-4 pt-4 pb-4'>
                <div className='w-full'><b>Type:</b> {report.payload.reportType}</div>
                <div className='w-full'><b>Message:</b> message</div>
            </div>
            <div className='col-span-3 pt-4 pb-4'>
                <div className='w-full'><button type='button' className='mb-2 bg-gray-500 w-28 hover:bg-blue-700 text-white font-bold py-0.5 rounded' onClick={()=>{sendBlockRequest(report.id)}}>Block</button></div>
                <div className='w-full'><button type='button' className='bg-gray-500 w-28 hover:bg-blue-700 text-white font-bold py-0.5 rounded' onClick={()=>{sendResolveRequest(report.id)}}>Resolve</button></div>
            </div>
        </div>
    );
}

export default ReportsTableRow;
