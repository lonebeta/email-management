import React, { useState, useEffect } from 'react';
import {ReportsInterface, ElementInterface} from 'components/Interface/Reports';
import ReportsTableRow from './ReportsTableRow';

const ReportsTable: React.FC = (props) => {
    const [reports, setReports] = useState<ReportsInterface | undefined>(undefined);

    useEffect(() => {
        fetch(`http://localhost:8000/reports`, { 
          method: 'get',
          headers: new Headers({
            'Content-Type':'application/json'
          })
        })
        .then(response => response.json())
        .then((data:ReportsInterface) => setReports(data));
    }, []);

    return (
        <div className='reports-table m-auto' style={{width:800}}>
            {reports && reports.elements && reports.elements.map((report:ElementInterface) => {
                return (
                    <ReportsTableRow
                        id={report.id}
                        source={report.source} 
                        sourceIdentityId={report.sourceIdentityId}
                        reference={report.reference} 
                        state={report.state}
                        payload={report.payload}
                        created={report.created}
                    />
                )
            })}
        </div>
    );
}
export default ReportsTable;
