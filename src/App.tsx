import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReportsTable from 'components/ReportsTable';

function App() {
  return (
    <div className="App">
      <h1 className="text-3xl font-bold underline">
        Reports
      </h1>
      <ReportsTable />
    </div>
  );
}

export default App;
